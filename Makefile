.PHONY:	deb rpm

deb:
	docker run --rm -v `pwd`:/project -w /project -it ubuntu:20.04 bash

rpm:
	docker run --rm -v `pwd`:/project -w /project -it centos:7 bash
