#!/usr/bin/env bash

apt-get update
apt-get install -y apt-transport-https lsb-release software-properties-common
add-apt-repository universe
add-apt-repository ppa:certbot/certbot
apt-get update
apt-get install -y certbot python3-certbot-nginx
