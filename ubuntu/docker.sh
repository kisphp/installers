#!/usr/bin/env bash

apt-get update
apt-get install -y apt-transport-https ca-certificates lsb_release curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce docker-compose
#systemctl status docker
sleep 10

DEFAULT_USER=$(cat /etc/passwd | grep 1000 | cut -d':' -f1)
if [[ ! -z "${DEFAULT_USER}" ]]; then
    echo "--- Add docker group to current user ---"
    sudo usermod -aG docker $DEFAULT_USER
fi
