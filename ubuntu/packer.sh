#!/bin/bash

# add the hashicorp GPG ket
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -

# add the official hashicorp linux repository
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

# update and install
sudo apt-get update && sudo apt-get install -y packer

# verify installation
packer version
