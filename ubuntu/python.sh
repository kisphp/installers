#!/usr/bin/env bash

# Configure noninteractive mode system wide
echo "export DEBIAN_FRONTEND=noninteractive" >> ~/.bashrc
export DEBIAN_FRONTEND=noninteractive

# Configure timezone for Berlin
sudo cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime

apt-get update
apt-get install -y python3 python3-pip python3-venv
ln -s /usr/bin/python3 /usr/bin/python
ln -s /usr/bin/pip3 /usr/bin/pip

which python3
which python

which pip3
which pip

python --version
pip --version
