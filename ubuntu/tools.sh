#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

apt-get update

apt-get install -y tzdata curl vim wget tree jq ncdu sudo net-tools telnet whois dnsutils pwgen unzip

ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime

dpkg-reconfigure --frontend noninteractive tzdata
