#!/usr/bin/env bash

# Dotfiles
curl https://gitlab.com/kisphp/dotfiles/raw/main/install.sh | bash -

# Tools
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/tools.sh | sudo bash -

# AWS CLI
curl -s https://gitlab.com/kisphp/installers/raw/main/awscli.sh | sudo bash -

# Kubectl
curl -s https://gitlab.com/kisphp/installers/raw/main/kubectl.sh | sudo bash -

# Helm 3
curl -s https://gitlab.com/kisphp/installers/raw/main/helm.sh | sudo bash -

# Terraform
curl -s https://gitlab.com/kisphp/installers/raw/main/terraform.sh | sudo bash -

# Terragrunt
curl -s https://gitlab.com/kisphp/installers/raw/main/terragrunt.sh | sudo bash -

# Docker
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/docker.sh | sudo bash -
