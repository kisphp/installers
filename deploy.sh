#!/usr/bin/env bash

echo -e "--- Download deployment script ---\n"
curl -s https://gitlab.com/kisphp/installers/raw/main/libs/dep.sh > dep.sh

echo -e "--- Make deployment script executable ---\n"
chmod u+x dep.sh
