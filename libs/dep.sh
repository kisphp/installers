#!/usr/bin/env bash

PROJECT="${1}"
BRANCH="${2}"

if [[ -z $BRANCH ]]; then
    BRANCH='master'
fi

# stop on errors
set -e

GIT=$(which git)
CWD=$(pwd)

if [[ -z $PROJECT ]]; then
    echo "You must provide project directory name"

    exit 0
fi

if [[ ! -d "${PROJECT}" ]]; then
    echo "Directory ${PROJECT} does not exists"

    exit 0
fi

# switch to project directory
cd "${PROJECT}"

# cancel all changes
$GIT reset --hard
# switch to master branch
$GIT checkout master
# get latest changes
$GIT pull --rebase -v
# switch to specific commit
$GIT checkout $BRANCH

if [[ -f build.sh ]]; then
    # run build script inside project
    ./build.sh
    # save timestamp of last deployment
    date +%s > last_deploy.time
else
    echo "Build script does not exists"
fi

cd "${CWD}"
