#!/usr/bin/env bash

CURRENT_JENKINS_VERSION=$(java -jar jenkins.war --version)

FG_RED="\033[31m"
FG_GREEN="\033[32m"
FG_YELLOW="\033[33m"

NC="\033[0m" # reset

NEW_JENKINS_VERSION="${1}"
DOWNLOAD_URL="http://updates.jenkins-ci.org/download/war/${NEW_JENKINS_VERSION}/jenkins.war"

if [[ "${CURRENT_JENKINS_VERSION}" == "${NEW_JENKINS_VERSION}" ]]; then
    echo -e $FG_RED
    echo -e "Jenkins is already at this version. No changes needed"
    echo -e $NC
    exit 1
fi

echo -e "Current jenkins version: ${FG_GREEN}${CURRENT_JENKINS_VERSION}${NC}"

#
# Check if jenkins.war exists in current directory
#
if [[ ! -f jenkins.war ]]; then
    echo -e $FG_RED
    echo -e "jenkins.war file does not exists in current directory"
    echo -e "On ubuntu, you can find the file in ${FG_YELLOW}/usr/share/jenkins/${FG_RED} directory"
    echo -e $NC
    exit 1
fi

echo "Download URL: ${DOWNLOAD_URL}"

if [[ "${NEW_JENKINS_VERSION}" == "" ]]; then
    echo -e "${FG_RED}New jenkins version parameter was not provided${NC}"
    exit 1
fi

if [[ -f jenkins.war ]]; then
    echo "Archiving current jenkins version"
    tar -czvf "jenkins.war.${CURRENT_JENKINS_VERSION}.tar.gz" jenkins.war
    rm jenkins.war
else
    echo -e "${FG_RED}Current jenkins.war was not found${NC}"
    exit 1
fi

echo "Download new version of jenkins"
wget $DOWNLOAD_URL

if [[ $(java -jar jenkins.war --version) == "${NEW_JENKINS_VERSION}" ]]; then
echo -e "${FG_GREEN}Jenkins was upgraded successfully to version ${NEW_JENKINS_VERSION}${NC}"
else
    echo -e "${FG_RED}Jenkins was not upgraded successfully and was rolled back to version ${CURRENT_JENKINS_VERSION}${NC}"
    tar -xzvf "jenkins.war.${CURRENT_JENKINS_VERSION}.tar.gz"
fi
