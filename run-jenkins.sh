#!/usr/bin/env bash

if [[ ! -d var/jenkins_home ]]; then
    sudo mkdir -p /var/jenkins_home
fi

sudo docker run \
    -d \
    -u root \
    -p 8080:8080 \
    -p 50000:50000 \
    -v `pwd`/var/jenkins_home:/var/jenkins_home \
    jenkins/jenkins:2.138.2
