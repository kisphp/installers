#!/usr/bin/env bash

echo "Please provide jenkins username: "
read USERNAME

echo "Please provide jenkins password: "
read PASSWORD

echo "Please provide jenkins host: "
read JENKINS_HOST

echo "Please provide jenkins port: "
read JENKINS_PORT

JENKINS_HOST="${USERNAME}:${PASSWORD}@${JENKINS_HOST}:${JENKINS_PORT}"

curl -sSL "https://$JENKINS_HOST/pluginManager/api/xml?depth=1&xpath=/*/*/shortName|/*/*/version&wrapper=plugins" | perl -pe 's/.*?<shortName>([\w-]+).*?<version>([^<]+)()(<\/\w+>)+/\1 \2\n/g'|sed 's/ /:/'
