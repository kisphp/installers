# Kisphp General installers for Ubuntu 20.04


### Devops tools

- dotfiles
- tools (tzdata, curl, wget, jq, net-tools, telnet, ncdu, and more)
- aws cli
- kubectl
- helm 3
- terraform
- terragrunt
- docker

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/devops.sh | bash -
```

### Upgrade jenkins version

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/upgrade-jenkins.sh | bash -s <new_jenkins_version>
```

### Install Deployment script

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/deploy.sh | bash -
```

### Install Composer

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/composer.sh | bash -
```

### Install [Certbot](https://certbot.eff.org/lets-encrypt/ubuntuxenial-nginx)

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/certbot.sh | sudo bash -
```

### Install [Ansible](https://docs.ansible.com/)

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/ansible.sh | sudo bash -
```

### Install [Docker](https://www.docker.com/) on Ubuntu

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/docker.sh | sudo bash -
```

### Install [Terraform](https://www.terraform.io) on Ubuntu

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/terraform.sh | sudo bash -
```

### Install [Terragrunt](https://www.terraform.io) on Ubuntu

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/terragrunt.sh | sudo bash -
```

### Install Java 11 SDK & JRE

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/java-11.sh | sudo bash -
```

### Install Jenkins

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/jenkins.sh | sudo bash -
```

### Run Jenkins in docker

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/run-jenkins.sh | sudo bash -
```

### Get Jenkins installed plugins list

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/get-jenkins-plugins-list.sh | bash -
```

### Install python 3 on ubuntu 20.04 LTS

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/python.sh | sudo bash -
```

### Install helm 3 on ubuntu 20.04 LTS

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/helm.sh | sudo bash -
```

### Install kubectl on ubuntu 20.04 LTS

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/kubectl.sh | sudo bash -
```

### Install awscli on ubuntu 20.04 LTS

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/awscli.sh | sudo bash -
```

### Install packer on ubuntu 20.04 LTS

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/packer.sh | sudo bash -
```

### Show OS statistics

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/stats.sh | sudo bash -
```

### Install ubuntu common tools

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/tools.sh | sudo bash -
```

### Fix ubuntu workspace shortcuts

```bash
curl -s https://gitlab.com/kisphp/installers/raw/main/ubuntu/ubuntu-workspace.sh | sudo bash -
```
