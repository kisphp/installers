#!/usr/bin/env bash

VERSION='1.9.5'

wget https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_amd64.zip
unzip terraform_${VERSION}_linux_amd64.zip
rm terraform_${VERSION}_linux_amd64.zip

mv terraform /usr/local/bin/

terraform version
