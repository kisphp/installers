#!/usr/bin/env bash

VERSION='0.35.16'

wget https://github.com/gruntwork-io/terragrunt/releases/download/v${VERSION}/terragrunt_linux_amd64 -O terragrunt
chmod +x terragrunt
mv terragrunt /usr/local/bin/

terragrunt
