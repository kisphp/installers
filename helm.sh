#!/usr/bin/env bash

HELM_VERSION='3.7.2'

wget https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz

tar -xzvf helm-v${HELM_VERSION}-linux-amd64.tar.gz

sudo mv linux-amd64/helm /usr/local/bin/helm

rm helm-v${HELM_VERSION}-linux-amd64.tar.gz
rm -rf linux-amd64/

helm version
