#!/usr/bin/env bash

OS_TYPE=$(uname -o)

BG_LABEL="\033[33m\033[40m"
BG_RED="\033[31m"
NC="\033[0m"

# get OS release
cat /etc/os-release | grep "NAME\|VERSION" | grep -v "VERSION_ID" | grep -v "PRETTY_NAME" > /tmp/os_release
OS_NAME=$(cat /tmp/os_release | grep -v "VERSION" | grep -v "CODENAME\|CPE" | cut -f2 -d\")
OS_VERSION=$(cat /tmp/os_release | grep -v "NAME" | grep -v "PROJECT\|PRODUCT" | cut -f2 -d\")
rm /tmp/os_release

ARCHITECTURE=$(uname -m)

KERNEL_RELEASE=$(uname -r)

INTERNAL_IPS=$(hostname -I)

EXTERNAL_IP="${BG_RED}CURL command is not installed${NC}"
IS_CURL=$(which curl)
if [[ $IS_CURL != '' ]]; then
    EXTERNAL_IP=$(curl -s ipecho.net/plain;echo)
fi

NAME_SERVERS=$(cat /etc/resolv.conf | sed '1 d' | awk '{print $2}')

UPTIME=$(uptime | awk '{print $3,$4}' | cut -f1 -d,)

LOAD_AVERAGE=$(top -n 1 -b | grep 'load average' | awk '{print $10 $11 $12}')

echo ''
echo -e $BG_LABEL "OS Type:        " $NC $OS_TYPE
echo -e $BG_LABEL "OS Name:        " $NC $OS_NAME
echo -e $BG_LABEL "OS Version:     " $NC $OS_VERSION
echo -e $BG_LABEL "Architecture:   " $NC $ARCHITECTURE
echo -e $BG_LABEL "Kernel release: " $NC $KERNEL_RELEASE
echo -e $BG_LABEL "Internal IPs:   " $NC $INTERNAL_IPS
echo -e $BG_LABEL "External IP:    " $NC $EXTERNAL_IP
echo -e $BG_LABEL "Name servers:   " $NC $NAME_SERVERS
echo -e $BG_LABEL "Uptime:         " $NC $UPTIME
echo -e $BG_LABEL "Load average:   " $NC $LOAD_AVERAGE

echo ''
echo -e $BG_LABEL "Memory usage:   " $NC
free -h

echo ''
echo -e $BG_LABEL "Disk usage:     " $NC
df -h | grep -v 'none'

echo ''
